import time
import markdown
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    readme_file = open("README.md", "r")
    md_template_string = markdown.markdown(
        readme_file.read(), extensions=["fenced_code"]
    )

    return md_template_string, 200

@app.route('/healthz')
def healthz():
    return dict(message="ok"), 200

@app.route('/api')
def api():
    return dict(message="Automate all the things!", timestamp=int(time.time())), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0')